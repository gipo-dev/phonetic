<?php


namespace App\Services\Phonetic;


class PhoneticService
{
    /**
     * @var IPhoneticService
     */
    protected static $service;

    /**
     * @param IPhoneticService $service
     */
    public static function setService(IPhoneticService $service)
    {
        self::$service = $service;
    }

    /**
     * @return IPhoneticService
     */
    public static function service()
    {
        return self::$service;
    }
}
