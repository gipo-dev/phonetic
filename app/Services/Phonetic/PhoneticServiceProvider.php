<?php


namespace App\Services\Phonetic;


use Illuminate\Support\ServiceProvider;

class PhoneticServiceProvider extends ServiceProvider
{
    public function boot()
    {
        PhoneticService::setService(new PhoneticOnlineRu());
    }
}
