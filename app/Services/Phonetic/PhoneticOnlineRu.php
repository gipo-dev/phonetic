<?php


namespace App\Services\Phonetic;


use App\Models\Cache;
use App\Models\Word;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;

class PhoneticOnlineRu implements IPhoneticService
{
    const ADDR = 'https://phoneticonline.ru/';

    /**
     * @inheritDoc
     */
    public function handle(Word $word, $accent)
    {
        $cache = Cache::find(self::class, $word->id);
        if ($cache && !$accent) {
            return $cache->data;
        } else
            return $this->handleRemote($word->word, $accent, $word);
    }

    private function handleRemote($word, $accent, $model)
    {
        if ($accent) {
            $response = (new Client)->request('POST', self::ADDR . 'word.json', [
                'form_params' => [
                    'q' => $word,
                    'pos' => $accent,
                ],
                'allow_redirects' => true,
            ]);
            $accent = json_decode($response->getBody()->getContents())->text;
        }
        if ($accent) {
            $response = (new Client)->request('POST', self::ADDR . $word, [
                'form_params' => [
                    'q' => $word,
                    'id' => $accent,
                ],
                'allow_redirects' => true,
            ]);
        } else {
            $response = (new Client)->request('POST', self::ADDR . 'search.html', [
                'form_params' => [
                    'q' => $word,
                ],
                'allow_redirects' => true,
            ]);
        }

        $dom = (new Dom())->loadStr($response->getBody()->getContents());
        $dom = $dom->find('.container')[1];
        $dom->setAttribute('class', 'result');
        $dom->find('h1')->delete();
        $dom->find('.text-muted.small.hidden-print')->delete();
        $dom->find('#vk_comments')->delete();
        $dom->find('.well.hidden-print')->delete();
        $words = [];
        foreach ($dom->find('.nav-words a') as $_word)
            $words[] = $_word->text;
        $dom->find('.nav-words')->delete();
        $dom->find('[data-ad-client]')->delete();
        $dom->find('[data-ad-client]')->delete();

        $html = $dom->outerHtml;
        $html = str_replace('(ударения были указаны пользователями сайта).', '', $html);

        $phonetic = new PhoneticWord();
        $phonetic->data = $html;
        $phonetic->related = $words;

        if (!$accent)
            $cache = Cache::create([
                'item_class' => self::class,
                'item_id' => $model->id,
                'data' => $phonetic,
            ]);

        return $phonetic;
    }
}
