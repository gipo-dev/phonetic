<?php

namespace App\Orchid\Screens\Spelling;

use App\Models\Spelling;
use App\Orchid\Filters\SpellingFilter;
use App\Orchid\Layouts\Spelling\SpellingFilterLayout;
use App\Orchid\Layouts\Spelling\SpellingListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class SpellingListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Правописание';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'words' => Spelling::filters()
                ->filtersApply([SpellingFilter::class])
                ->defaultSort('title')
                ->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Создать')
                ->icon('plus')
                ->type(Color::SUCCESS())
                ->route('platform.spelling.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SpellingFilterLayout::class,
            SpellingListLayout::class,
        ];
    }
}
