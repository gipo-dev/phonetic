<?php

namespace App\Orchid\Screens\Spelling;

use App\Http\Requests\Admin\SpecialWordRequest;
use App\Http\Requests\Admin\SpellingRequest;
use App\Models\Spelling;
use App\Orchid\Layouts\Spelling\SpellingCreateLayout;
use App\Orchid\Layouts\Spelling\SpellingListener;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class SpellingCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Создать новое правописание';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SpellingListener::class,
            SpellingCreateLayout::class,
        ];
    }

    /**
     * @param SpecialWordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(SpellingRequest $request)
    {
        $slug = Str::slug($request->word['title']);
        if (Spelling::where('slug', $slug)->exists()) {
            $slug .= '_' . now()->second;
        }
        $word = Spelling::create(
            array_filter(
                array_merge($request->word, ['slug' => $slug])
            )
        );
        return redirect()->route('platform.spelling.edit', $word->id);
    }

    /**
     * @param $variants
     * @return array
     */
    public function asyncSetVariants($variants)
    {
        $variants_array = (new Spelling(['variants' => $variants]))->getVariantsArray();
        return [
            'word.variants' => $variants,
            'variants' => $variants_array,
        ];
    }
}
