<?php

namespace App\Orchid\Screens\Spelling;

use App\Models\SpecialWord;
use App\Models\Spelling;
use App\Models\Word;
use App\Orchid\Layouts\SpecialWord\SpecialWordEditLayout;
use App\Orchid\Layouts\Spelling\SpellingEditLayout;
use App\Orchid\Layouts\Spelling\SpellingListener;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Card;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Alert;

class SpellingEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование';

    /**
     * Query data.
     *
     * @return array
     */
    public function query($word): array
    {
        $word = Spelling::find($word);
        return [
            'word' => $word,
            'variants' => $word->getVariantsArray(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Удалить')
                ->type(Color::DANGER())->icon('trash')
                ->confirm('Вы действительно желаете удалить данный ключ?')
                ->method('delete'),
            Link::make('Перейти')
                ->type(Color::INFO())->icon('eye')
                ->route('open', ['type' => 'spelling', 'id' => \request()->route()->parameter('word')])
                ->target('_blank'),
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('update'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SpellingListener::class,
            SpellingEditLayout::class,
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function open(Request $request)
    {
        return redirect(route('spelling', $request->word['slug']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $word = Spelling::find($request->word['id']);
        $word->update(array_filter($request->word));
        Alert::success('Изменения успешно сохранены');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $word = Spelling::find($request->word['id']);
        $word->delete();
        Alert::warning('Удалено');
        return redirect(route('platform.spelling.list'));
    }

    /**
     * @param $variants
     * @return array
     */
    public function asyncSetVariants($variants)
    {
        $variants_array = (new Spelling(['variants' => $variants]))->getVariantsArray();
        return [
            'word.variants' => $variants,
            'variants' => $variants_array,
        ];
    }
}
