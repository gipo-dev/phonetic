<?php

namespace App\Orchid\Screens\Word;

use App\Models\Word;
use App\Orchid\Filters\WordFilter;
use App\Orchid\Layouts\Word\WordFilterLayout;
use App\Orchid\Layouts\Word\WordListLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class WordListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Фонетический рабор слов';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'words' => Word::filters()
                ->filtersApply([WordFilter::class])
                ->defaultSort('word')
                ->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Создать')
                ->icon('plus')
                ->type(Color::SUCCESS())
                ->route('platform.word.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            WordFilterLayout::class,
            WordListLayout::class,
        ];
    }
}
