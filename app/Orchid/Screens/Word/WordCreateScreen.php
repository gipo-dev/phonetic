<?php

namespace App\Orchid\Screens\Word;

use App\Http\Requests\Admin\WordRequest;
use App\Models\Cache;
use App\Models\Word;
use App\Orchid\Layouts\Word\WordCreatePhonetic;
use App\Orchid\Layouts\Word\WordCreateRows;
use App\Services\Phonetic\HowToAllCom;
use App\Services\Phonetic\PhoneticOnlineRu;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class WordCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Новый фонетический разбор';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            WordCreateRows::class,
            WordCreatePhonetic::class,
        ];
    }

    public function save(WordRequest $request)
    {
        $word = Word::create($request->word);
        $cache = new Cache([
            'item_class' => PhoneticOnlineRu::class,
            'item_id' => $word->id,
        ]);
        $cache->data = (object)[
            'data' => $request->phonetic ?? '',
        ];
        $cache->save();

        $cache = new Cache([
            'item_class' => HowToAllCom::class,
            'item_id' => $word->id,
        ]);
        $cache->data = $request->additional;
        $cache->save();
        return redirect()->route('platform.word.edit', $word);
    }
}
