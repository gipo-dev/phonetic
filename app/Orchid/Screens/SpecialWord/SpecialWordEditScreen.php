<?php

namespace App\Orchid\Screens\SpecialWord;

use App\Http\Requests\Admin\SpecialWordRequest;
use App\Models\SpecialWord;
use App\Models\Word;
use App\Orchid\Layouts\SpecialWord\SpecialWordEditLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class SpecialWordEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(SpecialWord $word): array
    {
        return [
            'word' => $word,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Удалить')
                ->type(Color::DANGER())->icon('trash')
                ->confirm('Вы действительно желаете удалить данный ключ?')
                ->method('delete'),
            Link::make('Перейти')
                ->type(Color::INFO())->icon('eye')
                ->route('open', ['type' => 'special', 'id' => \request()->route()->parameter('word')])
                ->target('_blank'),
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('update'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SpecialWordEditLayout::class,
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function open(Request $request)
    {
        return redirect(route('special', $request->word['slug']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SpecialWordRequest $request)
    {
        $word = SpecialWord::find($request->word['id']);
        $word->update($request->word);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $word = SpecialWord::find($request->word['id']);
        $word->delete();
        return redirect(route('platform.specials.list'));
    }
}
