<?php

namespace App\Orchid\Screens\SpecialWord;

use App\Http\Requests\Admin\SpecialWordRequest;
use App\Models\SpecialWord;
use App\Orchid\Layouts\SpecialWord\SpecialWordCreateLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Color;

class SpecialWordCreateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Создать проверочное слово';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->type(Color::SUCCESS())->icon('paper-plane')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SpecialWordCreateLayout::class,
        ];
    }

    /**
     * @param SpecialWordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(SpecialWordRequest $request)
    {
        $slug = Str::slug($request->word['word']);
        if (SpecialWord::where('slug', $slug)->exists()) {
            $slug .= '_' . now()->second;
        }
        $word = SpecialWord::create(
            array_filter(
                array_merge($request->word, ['slug' => $slug])
            )
        );
        return redirect()->route('platform.specials.edit', $word);
    }
}
