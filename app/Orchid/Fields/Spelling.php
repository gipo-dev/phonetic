<?php


use Orchid\Screen\Field;

class Spelling extends Field
{
    protected $view = 'orchid.fields.spelling';

    protected $attributes = [
        'class'    => 'form-control',
        'datalist' => [],
    ];
}
