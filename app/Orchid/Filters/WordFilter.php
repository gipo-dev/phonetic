<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;

class WordFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = ['word'];

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('word', 'like', $this->request->get('word') . '%');
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Input::make('word')
                ->type('text')
                ->value($this->request->get('word'))
                ->placeholder('Слово...')
//                ->title('Фильтр по слову')
        ];
    }

    public function name(): string
    {
        return 'Слово';
    }
}
