<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;

class SpellingFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = ['title'];

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('title', 'like', '%' . $this->request->get('title') . '%');
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Input::make('title')
                ->type('text')
                ->value($this->request->get('title'))
                ->placeholder('Слово...')
//                ->title('Фильтр по слову')
        ];
    }

    public function name(): string
    {
        return 'Слово';
    }
}
