<?php

namespace App\Orchid\Layouts\SpecialWord;

use App\Orchid\Screens\SpecialWord\SpecialWordEditScreen;
use Leshkens\OrchidTinyMCEField\TinyMCE;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class SpecialWordEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Данные';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $fields = [
            Input::make('word.meta_title')
                ->title('Мета-заголовок')
                ->maxlength(255)
                ->style('max-width:100%'),
            Group::make([
                Input::make('word.h1')
                    ->title('Заголовок (H1)')
                    ->maxlength(255),
                Input::make('word.word')
                    ->title('Ключевое слово')
                    ->maxlength(255)
                    ->required(),
                Input::make('word.meta_description')
                    ->maxlength(255)
                    ->title('Мета-описание'),
            ]),
            TinyMCE::make('word.data')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Фонетический разбор'),
            Input::make('word.id')
                ->hidden(),
            Input::make('word.slug')
                ->hidden(),
        ];

        $se = new SpecialWordEditScreen();
        $fields[] = Group::make($se->commandBar())
            ->autoWidth()
            ->set('align', 'justify-content-end');

        return $fields;
    }
}
