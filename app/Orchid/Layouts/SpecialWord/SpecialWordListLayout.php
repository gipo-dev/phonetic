<?php

namespace App\Orchid\Layouts\SpecialWord;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class SpecialWordListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'words';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('word', 'Слово')
                ->render(function ($word) {
                    return Link::make($word->word)
                        ->route('platform.specials.edit', $word);
                })->sort(),

            TD::make('id', '')
                ->render(function ($word) {
                    return Link::make('Ред.')
                        ->icon('pencil')
                        ->route('platform.specials.edit', $word);
                })->width(130)
                ->align(TD::ALIGN_RIGHT),
        ];
    }
}
