<?php

namespace App\Orchid\Layouts\Spelling;

use Leshkens\OrchidTinyMCEField\TinyMCE;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class SpellingCreateLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Данные';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Input::make('word.meta_title')
                ->title('Мета-заголовок')
                ->style('max-width:100%'),
            Group::make([
                Input::make('word.h1')
                    ->title('Заголовок (H1)'),
                Input::make('word.title')
                    ->title('Ключевое слово')
                    ->required(),
                Input::make('word.meta_description')
                    ->title('Мета-описание'),
            ]),
            TinyMCE::make('word.determination')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Определение'),
            TinyMCE::make('word.spelling')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Правописание'),
            TinyMCE::make('word.example')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Примеры'),
        ];
    }
}
