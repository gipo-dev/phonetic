<?php

namespace App\Orchid\Layouts\Spelling;

use App\Orchid\Filters\SpellingFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class SpellingFilterLayout extends Selection
{
    public $template = self::TEMPLATE_LINE;

    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            SpellingFilter::class,
        ];
    }
}
