<?php

namespace App\Orchid\Layouts\Spelling;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Support\Color;

class SpellingListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'words';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('title', 'Слово')
                ->render(function ($word) {
                    return Link::make($word->title)
                        ->route('platform.spelling.edit', $word->id);
                })->sort(),

            TD::make('id', '')
                ->render(function ($word) {
                    return Link::make('Ред.')
                        ->icon('pencil')
                        ->route('platform.spelling.edit', $word->id);
                })->width(130)
                ->align(TD::ALIGN_RIGHT),
        ];
    }
}
