<?php

namespace App\Orchid\Layouts\Spelling;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\ViewField;
use Orchid\Screen\Layout;
use Orchid\Screen\Layouts\Listener;

class SpellingListener extends Listener
{
    /**
     * List of field names for which values will be listened.
     *
     * @var string[]
     */
    protected $targets = [
        'word.variants',
    ];

    /**
     * What screen method should be called
     * as a source for an asynchronous request.
     *
     * The name of the method must
     * begin with the prefix "async"
     *
     * @var string
     */
    protected $asyncMethod = 'asyncSetVariants';

    /**
     * @return Layout[]
     */
    protected function layouts(): array
    {
        return [
            \Orchid\Support\Facades\Layout::rows([
                TextArea::make('word.variants')
                    ->title('Варианты написания')
                    ->rows(5)
                    ->popover("Каждый вариант с новой строки. \nПравильный вариант первая строка.\nВыделять ошибку во[т т]ак."),
            ]),
            \Orchid\Support\Facades\Layout::view('orchid.fields.spelling'),
        ];
    }
}
