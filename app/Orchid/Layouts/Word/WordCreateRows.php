<?php

namespace App\Orchid\Layouts\Word;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class WordCreateRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Данные';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Input::make('word.meta_title')
                ->title('Мета-заголовок')
                ->maxlength(255)
                ->style('max-width:100%'),
            Group::make([
                Input::make('word.h1')
                    ->title('Заголовок (h1)')
                    ->maxlength(255),
                Input::make('word.word')
                    ->title('Ключевое слово')
                    ->maxlength(255)
                    ->required(),
                Input::make('word.meta_description')
                    ->maxlength(255)
                    ->title('Мета-описание'),
            ]),

            Input::make('word.views')
                ->value(0)
                ->hidden(),
            DateTimer::make('word.created_at')
                ->value(now()->format('d.m.Y'))
                ->format('d.m.Y')
                ->hidden(),
        ];
    }
}
