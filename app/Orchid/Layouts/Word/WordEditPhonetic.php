<?php

namespace App\Orchid\Layouts\Word;

use App\Orchid\Screens\SpecialWord\SpecialWordEditScreen;
use App\Orchid\Screens\Word\WordEditScreen;
use Leshkens\OrchidTinyMCEField\TinyMCE;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Layouts\Rows;

class WordEditPhonetic extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Разбор слова';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $titles = [
            'morph' => 'Морфологический разбор',
            'phon' => 'Звуко-буквенный разбор',
            'meaning' => 'Значение слова',
        ];
        $rows = [
            TinyMCE::make('phonetic')
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title('Фонетический разбор'),
        ];

        $additionals = $this->query->get('additional') ?? ['morph' => '', 'phon' => '', 'meaning' => ''];
        foreach ($additionals as $k => $item) {
            $rows[] = TinyMCE::make('additional.' . $k)
                ->theme('modern')
                ->config(['content_css' => [
                    '/css/ui.css',
                    '/phonetic/css/ui.css',
                ]])
                ->title($titles[$k] ?? '');
        }



        $se = new WordEditScreen();
        $rows[] = Group::make($se->commandBar())
            ->autoWidth()
            ->set('align', 'justify-content-end');
        return $rows;
    }
}
