<?php

namespace App\Orchid\Layouts\Word;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class WordEditRows extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Данные';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Input::make('word.meta_title')
                ->title('Мета-заголовок')
                ->maxlength(255)
                ->style('max-width:100%'),
            Group::make([
                Input::make('word.h1')
                    ->title('Заголовок (h1)')
                    ->maxlength(255),
                Input::make('word.word')
                    ->title('Ключевое слово')
                    ->maxlength(255)
                    ->required(),
                Input::make('word.meta_description')
                    ->maxlength(255)
                    ->title('Мета-описание'),
            ]),
            Group::make([
                Input::make('word.views')
                    ->title('Количество просмотров')
                    ->required(),
                DateTimer::make('word.created_at')
                    ->title('Дата создания')
                    ->placeholder('Дата создания')
                    ->format('d.m.Y')
                    ->allowInput()
                    ->required(),
            ]),
            Input::make('word.id')
                ->hidden(),
        ];
    }
}
