<?php

namespace App\Orchid\Layouts\Word;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Support\Color;

class WordListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'words';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('word', 'Слово')
                ->render(function ($word) {
                    return Link::make($word->word)
                        ->route('platform.word.edit', $word);
                })->sort(),

            TD::make('views', 'Просмотры')->sort()
                ->width(130)
                ->align(TD::ALIGN_RIGHT),

            TD::make('created_at', 'Дата создания')->render(function ($word) {
                return $word->created_at ? $word->created_at->format('d-m-Y') : '';
            })->width(120)
                ->align(TD::ALIGN_RIGHT)
                ->sort(),

            TD::make('id', '')
                ->render(function ($word) {
                    return Link::make('Ред.')
                        ->icon('pencil')
                        ->route('platform.word.edit', $word);
                })->width(130)
                ->align(TD::ALIGN_RIGHT),
        ];
    }
}
