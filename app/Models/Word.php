<?php

namespace App\Models;

use App\Services\Phonetic\HowToAllCom;
use App\Services\Phonetic\PhoneticService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Word extends Model
{
    use AsSource;
    use Filterable;
    use Linkable;
    use HasMeta;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'word',
        'views',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'word',
        'views',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->meta_patterns = [
            'title' => '{key} звуко-буквенный (фонетический) разбор слова',
            'description' => '{key} звуко-буквенный (фонетический) разбор слова',
            'h1' => 'Фонетический разбор слова - "{key}" (звуки и буквы)',
        ];
    }

    /**
     * @param $query
     * @param string $word
     * @return
     */
    public function scopeSearch($query, $word)
    {
        return $query->where('word', $word);
    }

    /**
     * @return \Illuminate\Http\Response|object
     */
    public function phonetic($accent = null)
    {
        try {
            $phonetic = [
                'additional' => (new HowToAllCom())->handle($this),
            ];
        } catch (\Exception $exception) {
            $phonetic = [
                'additional' => false,
            ];
        }
        try {
            $phonetic['phonetic'] = PhoneticService::service()->handle($this, $accent);
        } catch (\Exception $exception) {
            $phonetic['phonetic'] = false;
        }

        return $phonetic;
    }

    public function getWordAttribute()
    {
        return urldecode($this->attributes['word'] ?? '');
    }

    public function getSynonims($sinon)
    {
        return collect($sinon)->filter(function ($w) {
            return !Str::contains($w, ' ');
        });
    }
}
