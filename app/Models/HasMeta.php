<?php


namespace App\Models;


use stringEncode\Exception;

trait HasMeta
{
    /**
     * @var string
     */
    protected $meta_key = 'word';

    /**
     * @var string[]
     */
    protected $meta_patterns = [
        'title' => '{key}',
        'description' => '{key}',
        'h1' => '{key}',
    ];

    /**
     * @return string
     */
    public function getH1Attribute()
    {
        return $this->getMetaValue('h1', 'h1');
    }

    /**
     * @return string
     */
    public function getMetaTitleAttribute()
    {
        return $this->getMetaValue('meta_title', 'title');
    }

    /**
     * @return string
     */
    public function getMetaDescriptionAttribute()
    {
        return $this->getMetaValue('meta_description', 'description');
    }

    /**
     * @param string $key
     * @param string $pattern_key
     * @return string
     * @throws Exception
     */
    private function getMetaValue($key, $pattern_key)
    {
        if (!isset($this->meta_patterns[$pattern_key]))
            throw new Exception("Не установлен паттерн для [$pattern_key]");
        if (trim($this->attributes[$key] ?? '') !== '')
            return $this->attributes[$key];
        return str_replace('{key}', mb_ucfirst($this->{$this->meta_key}), $this->meta_patterns[$pattern_key]);
    }

    public function initMetaValues()
    {
        \Meta::set('title', $this->meta_title);
        \Meta::set('description', $this->meta_description);
    }
}
