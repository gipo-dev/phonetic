<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Linking extends Model
{
    protected $table = 'linking';

    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'items' => 'array',
    ];
}
