<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{

    /**
     * @var string[]
     */
    protected $guarded = [];

    /**
     * @param $query
     * @param string $key
     * @param string $default
     * @return void
     */
    public function scopeKey($query, $key, $default = '')
    {
        return $query->firstOrCreate([
            'name' => $key,
        ], [
            'value' => $default,
            'updated_at' => now()->subYear(),
        ]);
    }
}
