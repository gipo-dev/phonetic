<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cache extends Model
{
    protected $table = 'cache';

    protected $guarded = [];

    public function setDataAttribute($data)
    {
        $data = serialize($data);
        $this->attributes['data'] = gzencode($data, 9);
    }

    public function getDataAttribute()
    {
        $data = gzdecode($this->attributes['data']);
        return unserialize($data);
    }

    public static function find($class, $id)
    {
        return self::where('item_class', $class)->where('item_id', $id)->first();
    }
}
