<?php


namespace App\Models;


trait Linkable
{
    public function getRelated()
    {
        $related = Linking::where('class', self::class)->where('item_id', $this->id)->first();
        if (!$related) {
            $random = [];
            $max = (self::class)::count();
            for ($i = 0; $i < 20; $i++) {
                $random[] = rand(0, $max);
            }
            $related = Linking::create([
                'class' => self::class,
                'item_id' => $this->id,
                'items' => $random,
            ]);
        }
//        dd($this);
        return $this->getRelatedItems($related->items);
    }

    private function getRelatedItems($id)
    {
        return (self::class)::find($id);
    }
}
