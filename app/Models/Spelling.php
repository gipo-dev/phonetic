<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Spelling extends Model
{
    use AsSource;
    use Filterable;
    use Linkable;
    use HasMeta;

    protected $guarded = [];

    protected $allowedFilters = [
        'id',
        'title',
    ];

    protected $allowedSorts = [
        'id',
        'title',
    ];

    public $timestamps = false;

    protected $attributes = [
        'determination' => '',
        'spelling' => '',
        'example' => '',
    ];

//    protected $casts = [
//        'variants' => 'array',
//    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->meta_key = 'title';
        $this->meta_patterns = [
            'title' => '{key} - как правильно?',
            'description' => '{key} - как правильно?',
            'h1' => 'Как правильно пишется {key}',
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getTitleAttribute()
    {
        return html_entity_decode($this->attributes['title']);
    }

//    public function getDeterminationAttribute() {
//        dd($this->attributes['determination']);
//    }

    public function getVariantsArray($variants = false)
    {
        if (!$variants)
             $variants = $this->variants ?? '';
        $variants_array = [];
        foreach (explode(PHP_EOL, $variants) as $variant) {
            if (trim($variant) == '')
                continue;
            $variants_array[] = str_replace(
                ['[', ']'],
                ['<i class="diff">', '</i>'],
                $variant);
        }
        return $variants_array;
    }
}
