<?php


namespace App\Models;


class PopularWords
{
    const BY_VIEWS = 'views';
    const BY_DATE = 'created_at';

    public static function get($by = self::BY_VIEWS, $result = true)
    {
        $popular = Option::key('popular_' . $by);
        if ($popular->updated_at > now()->subHours(12) && trim($popular->value) != '') {

        } else {
            PopularWords::generate($popular, $by);
            $popular = Option::find($popular->id);
        }
        return Word::whereIn('id', explode(',', $popular->value))->get();
    }

    public static function getSpellings()
    {
        $key = str_replace('phonetic/', '', request()->path());
        $popular = Option::key('spellings_' . $key);
        if (trim($popular->value) == '') {
            $random = [];
            for ($i = 0; $i < 20; $i++) {
                $random[] = rand(0, 1349);
            }

            $popular->update([
                'value' => implode(',', Spelling::find($random)
                    ->take(10)->pluck('id')->toArray()),
                'updated_at' => now(),
            ]);
        }
        return Spelling::find(explode(',', $popular->value));
    }

    public static function generate($option, $by = self::BY_VIEWS)
    {
        $option->update([
            'value' => implode(',', Word::select('id')
                ->orderByDesc($by)
                ->limit(20)
                ->get()->pluck('id')->toArray()),
            'updated_at' => now(),
        ]);
    }
}
