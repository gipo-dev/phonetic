<?php

namespace App\Http\Controllers;

use App\Models\Spelling;
use Illuminate\Http\Request;

class SpellingController extends Controller
{
    public function list()
    {
        \Meta::set('title', "Правописание слов онлайн онлайн");
        \Meta::set('menu', 2);
        $words = Spelling::orderBy('title')->simplePaginate(20);
        return view('spelling_list', compact('words'));
    }

    public function show(Request $request, Spelling $word)
    {
        $word->initMetaValues();
        \Meta::set('menu', 2);
        return view('spelling', compact('word'));
    }
}
