<?php

namespace App\Http\Controllers;

use App\Events\WordSearch;
use App\Models\Letter;
use App\Models\SpecialWord;
use App\Models\Spelling;
use App\Models\Word;
use App\Services\Phonetic\HowToAllCom;
use App\Services\Phonetic\PhoneticService;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;

class WordController extends Controller
{
    /**
     * @param Request $request
     * @param string $word
     * @return \Illuminate\Contracts\View\View
     */
    public function result(Request $request, $word)
    {
        event(new WordSearch($word));
        $_word = Word::where('word', $word)->first();
        $_word->initMetaValues();
        \Meta::set('menu', 1);
        \Meta::set('canonical', \request()->url());

        $phonetic = $_word->phonetic($request->accent);

        if ($phonetic['additional'] === false && $phonetic['phonetic'] === false) {
            \Meta::set('title', 'Ошибка при разборе слова');
            Word::search($word)->delete();
            return response()->view('error')->setStatusCode(404);
        }

        return view('word', compact('phonetic', 'word', '_word'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function search(Request $request)
    {
        return redirect(route('word', ['word' => $request->q]));
    }

    public function words(Request $request, Letter $letter = null)
    {
        \Meta::set('menu', 1);
        \Meta::set('canonical', \request()->url());

        if ($letter)
            \Meta::set('title', ("Список слов на букву «" . mb_ucfirst($letter->letter) . "»"));

        if ($letter)
            $words = Word::where('word', 'like', $letter->letter . '%')->orderBy('word')->simplePaginate(100);
        else
            $words = null;

        return view('words', [
            'words' => $words,
            'letters' => Letter::get(),
            'currentLetter' => $letter,
            'h1' => $letter ? ("Список слов на букву «" . mb_ucfirst($letter->letter) . "»") : null,
        ]);
    }

    public function composition(Request $request, $word)
    {
        \Meta::set('title', "Разбор слова \"$word\" по составу онлайн");
        \Meta::set('menu', 1);
        $word = Word::where('word', $word)->first();
        try {
            $phonetic = (new HowToAllCom())->handle($word);
        } catch (\Exception $exception) {
            $phonetic = false;
        }
        if (!$word || !isset($phonetic['sost'])) {
            return response()->view('composition_404')->setStatusCode(404);
        }
        return view('composition', compact('word', 'phonetic'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function specials(Request $request)
    {
        \Meta::set('title', "Проверочные слова онлайн");
        \Meta::set('menu', 4);

        return view('specials', [
            'words' => SpecialWord::orderBy('word')->simplePaginate(20),
        ]);
    }

    /**
     * @param Request $request
     * @param $word
     * @return \Illuminate\Contracts\View\View
     */
    public function special(Request $request, $word)
    {
        $word = SpecialWord::where('slug', $word)->firstOrFail();
        $word->initMetaValues();
        \Meta::set('menu', 4);
        return view('special', compact('word'));
    }

    public function open(Request $request, $type, $id)
    {
        switch ($type) {
            case 'spelling':
                $word = Spelling::findOrFail($id);
                return redirect(route('spelling', $word));
            case 'special':
                $word = SpecialWord::findOrFail($id);
                return redirect(route('special', $word->slug));
            case 'word':
                $word = Word::findOrFail($id);
                return redirect(route('word', $word->word));
        }

    }
}
