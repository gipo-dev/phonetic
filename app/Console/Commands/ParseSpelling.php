<?php

namespace App\Console\Commands;

use App\Models\Accent;
use App\Models\Spelling;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ParseSpelling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:spells';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->getContent();
    }

    private function getContent()
    {
        $_rows = [];
        if (($handle = fopen(storage_path('ударение.csv'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                $num = count($data);
                $line = [];
                for ($c = 0; $c < $num; $c++) {
                    $line[] = $data[$c];
                }
                $_rows[] = $line;
            }
            fclose($handle);
        }

        $rows = [];
        foreach ($_rows as $row) {
            if ($row[1] == 'Тайтл') continue;

            $link = Arr::last(explode('/', Str::replaceLast('/', '', $row[0] ?? '')));
            try {
                Accent::create([
                    'title' => str_replace('Правильное ударение в словосочетании ', '', $row[5]),
                    'slug' => $link,
                    'determination' => $row[2],
                    'spelling' => $row[3],
                    'example' => $row[4],
                ]);
            } catch (\Exception $exception) {
            }

//            dd($row);
//
//            $link = Arr::last(explode('/', Str::replaceLast('/', '', $row[0] ?? '')));
//            try {
//
//                Spelling::create([
//                    'title' => $row[5],
//                    'slug' => $link,
//                    'determination' => $row[2],
//                    'spelling' => $row[3],
//                    'example' => $row[4],
//                ]);
//            } catch (\Exception $exception) {
//            }
        }
//        dd($rows);
    }
}
