<?php

namespace Database\Seeders;

use App\Models\Letter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LetterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (mb_str_split('абвгдежзиклмнопрстуфхцшщэюя') as $letter) {
            Letter::create([
                'letter' => $letter,
                'slug' => Str::slug($letter),
            ]);
        }
    }
}
