require('./bootstrap');
require('bootstrap/dist/js/bootstrap.bundle');

$(function () {
    $('#showAccentBox').click(function () {
        $(this).hide();
        $('#setAccentBox').show();
    });

    $('#setAccentBox .char').click(function () {
        let accent = $(this).attr('pos');
        window.location.href = window.location.pathname + '?accent=' + accent;
    });

    // $('.suffix').each(function (i, el) {
        // var width = $(el).innerWidth();
        // $(this).append('<span class="suffix-symbol" style="width: ' + width + ';height: ' + width + ';"></span>');
        // $(this).append('<img src="/suffix.svg">');
    // });
});
