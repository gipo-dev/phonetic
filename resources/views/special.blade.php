@extends('partials.body')

@section('content')
    <div class="card mt-4">
        {{--    <h1 class="card-header">"{!! mb_ucfirst($word->word) !!}" - проверочное слово</h1>--}}
        <h1 class="card-header">{!! $word->h1 !!}</h1>
        <div class="special-word">
            <div class="card-body">
                @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                    <a href="{{ route('platform.specials.edit', $word->id) }}" class="btn btn-primary btn-sm mb-2">Редактировать</a>
                @endif
                {!! str_replace(['href', '<button', '</button', '<span>.</span>'], ['', '<hidden', '</hidden', ''], \App\Services\Helper::close_tags($word->data)) !!}
            </div>
        </div>

        <h4 class="mt-4">Примеры других слов:</h4>
        <ul class="list related">
            @foreach(\App\Models\SpecialWord::where(function($q) use ($word) { $q->where('id', '>', $word->id)->where('id', '<=', $word->id + 15); })
                    ->orWhere(function($q) use ($word) { $q->where('id', '<', $word->id)->where('id', '>=', $word->id - 15); })->get() as $word)
                <li>
                    <a href="{{ route('special', $word->slug) }}">{!! $word->word !!}</a>
                </li>
            @endforeach
        </ul>

@endsection
