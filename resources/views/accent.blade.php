@extends('partials.body')

@section('content')
    <div class="card mt-4">
        <h1 class="card-header">Правильное ударение в слове {!! $word->title !!}?</h1>
        <div class="card-body">
            {!! \App\Services\Helper::close_tags($word->determination) !!}
        </div>
    </div>

    <div class="card">
        <h2 class="card-header">Правильное произношение {!! $word->title !!}</h2>
        <div class="card-body">
            {!! \App\Services\Helper::close_tags($word->spelling) !!}
        </div>
    </div>

    @if($word->example)
        <div class="card mb-4">
            <h2 class="card-header">Примеры использования слова {!! $word->title !!}</h2>
            <div class="card-body pb-4">
                <ul class="mb-2">{!! \App\Services\Helper::close_tags($word->example) !!}</ul>
            </div>
        </div>
    @endif

    <ul class="list related">
        @foreach($word->getRelated() as $word)
            <li>
                <a href="{{ route('accent', $word->slug) }}">{!! $word->title !!}</a>
            </li>
        @endforeach
    </ul>

@endsection
