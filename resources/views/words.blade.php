@extends('partials.body')

@section('content')
    <h1 class="mt-4 words_h1">{{ $h1 ?? 'Фонетический разбор слова онлайн' }}</h1>

    <ul class="list letters">
        @foreach($letters as $letter)
            <li>
                <a href="{{ route('words', $letter) }}" @if($currentLetter == $letter) class="_current" @endif>
                    {{ mb_ucfirst($letter->letter) }}
                </a>
            </li>
        @endforeach
    </ul>

    @if($words)
        <table class="table table-striped table-borderless border-bottom-0 mt-4 mb-4">
            @foreach($words as $word)
                <tr>
                    <td>
                        <a href="{{ route('word', $word->word) }}">
                            {{ mb_ucfirst(urldecode($word->word)) }}
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $words->links() }}
    @endif
@endsection
