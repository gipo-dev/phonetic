@include('partials.header')
@include('partials.navbar')

<div class="container content">
    <div class="row">
        <div class="d-lg-none nav-mobile"></div>
        <div class="col-md-9">
            @yield('content')
        </div>
        <div class="col-md-3 rmenu">
            @include('partials.rmenu')
        </div>

        <div class="col-12 footer">
            <ul id="menu-navigatsiya-1" class="nav container group">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-20"><a
                        href="https://literaguru.ru">Главная</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1082"><a
                        href="https://literaguru.ru/category/stati/">Статьи</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1089"><a
                        href="https://literaguru.ru/category/kunilingvist/">Кунилингвист</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor menu-item-has-children menu-item-1088">
                    <a href="https://literaguru.ru/category/obrazovanie/">Образование</a>
                    <ul class="sub-menu" style="display: none;">
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1092"><a
                                href="https://literaguru.ru/category/obrazovanie/sochineniya/">Сочинения</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-4211"><a
                                href="https://literaguru.ru/itogovoe-sochinenie-2017-2018-vsyo-o-nyom/">Итоговое
                                сочинение 2018-2019</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3924"><a
                                href="https://literaguru.ru/category/obrazovanie/analiz-proizvedenij/">Анализ
                                произведений</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1091"><a
                                href="https://literaguru.ru/category/obrazovanie/doklady/">Доклады</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1090"><a
                                href="https://literaguru.ru/category/obrazovanie/abiturientam/">Абитуриентам</a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent menu-item-10656">
                            <a href="https://literaguru.ru/category/obrazovanie/kratkie-soderzhaniya/">Краткие
                                содержания</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11100"><a
                        href="https://literaguru.ru/category/obrazovanie/biografii/">Биографии</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-883"><a
                        href="https://literaguru.ru/kontakty/">Контакты</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10208"><a
                        href="https://literaguru.ru/advertising/">Рекламодателям</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10322"><a
                        href="https://zen.yandex.ru/literaguru">Наш канал в Яндекс Дзен</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11085"><a
                        href="http://egevpare.ru/?utm_source=literaguru_ru&amp;utm_medium=cpc&amp;utm_campaign=text-link"><b><font
                                color="yellow">Курсы подготовки к ЕГЭ и ОГЭ</font></b></a></li>
            </ul>
        </div>
    </div>
</div>

@include('partials.footer')
