<nav class="navbar navbar-expand-lg navbar-light p-0">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 flex-column rmenu-nav">
            <li class="nav-item">
                <a class="nav-link @if(\Meta::get('menu') == 0) active @endif" aria-current="page"
                   href="{{ route('index') }}">Фонетический разбор</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(\Meta::get('menu') == 1) active @endif" aria-current="page"
                   href="{{ route('words') }}">Алфавитный указатель</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(\Meta::get('menu') == 2) active @endif" aria-current="page"
                   href="{{ route('spelling.list') }}">Правописание
                    слов</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(\Meta::get('menu') == 3) active @endif" aria-current="page"
                   href="{{ route('accent.list') }}">
                    Правильные ударения
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link @if(\Meta::get('menu') == 4) active @endif" aria-current="page" href="{{ route('special.list') }}">
                    Проверочные слова
                </a>
            </li> --}}
        </ul>
    </div>
</nav>

<div class="rmenu-header">
    Разобрать слово
</div>
<form action="{{ route('search') }}" method="post">
    @csrf
    <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Введите слово" aria-label="Recipient's username"
               aria-describedby="button-addon2" name="q" required>
        <button class="btn btn-success" type="submit" id="button-addon2">Разобрать</button>
    </div>
</form>
{{--<p class="h5 mt-4">Популярные слова</p>--}}
<div class="rmenu-header">
    Популярные слова
</div>
<ul class="list popular">
    @foreach(\App\Models\PopularWords::get() as $popular)
        <li>
            <a href="{{ route('word', $popular->word) }}">{{ $popular->word }}</a>
        </li>
    @endforeach
</ul>
@if(request()->route()->getName() != 'spelling')
    <div class="rmenu-header">
        Как правильно пишется?
    </div>
    <ul class="list popular">
        @foreach(\App\Models\PopularWords::getSpellings() as $rand)
            <li>
                <a href="{{ route('spelling', $rand->slug) }}">{!! $rand->title !!}</a>
            </li>
        @endforeach
    </ul>
@endif
