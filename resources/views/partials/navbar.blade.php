<div class="container px-0">
    <a href="{{ route('index') }}" class="header-image">
        <img src="https://literaguru.ru/wp-content/uploads/2017/01/cropped-default-header-280.jpg" alt="Литерагуру">
    </a>
</div>

<div class="container d-none d-lg-block">
    <div class="row">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark navbar-green">
            <ul class="navbar-nav">
                <li class="nav-link">
                    <a href="https://literaguru.ru/">Главная</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/category/stati/">Статьи</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/category/kunilingvist/">Кунилингвист</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/category/obrazovanie/">Образование</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/category/obrazovanie/biografii/">Биографии</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/kontakty/">Контакты</a>
                </li>
                <li class="nav-link">
                    <a href="https://literaguru.ru/advertising/">Рекламодателям</a>
                </li>
                <li class="nav-link">
                    <a href="https://zen.yandex.ru/literaguru">Наш канал в Яндекс Дзен</a>
                </li>
                <li class="nav-link accent">
                    <a href="http://egevpare.ru/?utm_source=literaguru_ru&utm_medium=cpc&utm_campaign=text-link">Курсы
                        подготовки к ЕГЭ и ОГЭ</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

{{--<div class="container">--}}
{{--    <section class="section-nav">--}}
{{--        <nav class="navbar navbar-expand-lg navbar-light bg-light">--}}
{{--            <a class="navbar-brand fw-bold" href="/">Фонетический разбор слов</a>--}}
{{--            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"--}}
{{--                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"--}}
{{--                    aria-expanded="false" aria-label="Toggle navigation">--}}
{{--                <span class="navbar-toggler-icon"></span>--}}
{{--            </button>--}}
{{--            <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--                <ul class="navbar-nav me-auto mb-2 mb-lg-0">--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" aria-current="page" href="/">Главная</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" aria-current="page" href="{{ route('words') }}">Алфавитный указатель</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" aria-current="page" href="{{ route('spelling.list') }}">Правописание--}}
{{--                            слов</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" aria-current="page" href="{{ route('accent.list') }}">Правописаные--}}
{{--                            ударения</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </nav>--}}
{{--    </section>--}}
{{--</div>--}}
