@extends('partials.body')

@section('content')
    <div class="alert alert-danger mt-3">
        <h4 class="mt-0">Ошибка при разборе слова</h4>
        Мы делаем разбор только русских слов. Не допускаются в словах иностранные буквы, знаки пунктуации, цифры и
        другие символы, не являющиеся русскими буквами. С учётом этого попробуйте ещё раз поискать слово или
        воспользуйтесь алфавитным указателем.
    </div>
    
    <form id="search-composition" data-url="{{ route('composition', 'kkk') }}" class="input-group mb-3">
        <input type="text" id="composition-word" class="form-control" value="{{ request()->word ?? '' }}" placeholder="Разобрать другое слово">
        <button class="btn btn-success" type="submit" id="button-addon2">Разобрать</button>
    </form>

@push('scripts')
<script>
    $(function() {
        $('#search-composition').submit(function (e) {
            e.preventDefault();
            var url = $(this).data('url').replace('kkk', '');
            window.location.href = url + $('#composition-word').val();
        });
    });
</script>
@endpush

    <h4 class="mt-5">Примеры других слов с разборами:</h4>
        <ul class="list related">
            @php
            $ids = [];
            for($i = 0; $i < 30; $i++)
                $ids[] = rand(0, 100000);
            @endphp
            @foreach(\App\Models\Word::find($ids) as $word)
                <li>
                    <a href="{{ route('composition', $word->word) }}">{!! $word->word !!}</a>
                </li>
            @endforeach
        </ul>
@endsection
