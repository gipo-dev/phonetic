@extends('partials.body')

@section('content')
    <h1 class="h3 mt-5 mb-3">Фонетический разбор слов онлайн</h1>
    <form action="{{ route('search') }}" method="post">
        @csrf
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Введите слово" aria-label="Recipient's username"
                   aria-describedby="button-addon2" name="q" required>
            <button class="btn btn-success" type="submit" id="button-addon2">Разобрать</button>
        </div>
    </form>
    <div class="mt-5">
        <h4 class="mt-4">Фонетический разбор слов:</h4>
        <ul class="list related">
            @php
            $ids = [];
            for($i = 0; $i < 30; $i++)
                $ids[] = rand(0, 100000);
            @endphp
            @foreach(\App\Models\Word::find($ids) as $word)
                <li>
                    <a href="{{ route('word', $word->word) }}">{!! $word->word !!}</a>
                </li>
            @endforeach
        </ul>
        
        <h4 class="mt-4">Правописание слов:</h4>
        <ul class="list related">
            @php
            $ids = [];
            for($i = 0; $i < 30; $i++)
                $ids[] = rand(0, 1350);
            @endphp
            @foreach(\App\Models\Spelling::find($ids) as $word)
                <li>
                    <a href="{{ route('spelling', $word->slug) }}">{!! $word->title !!}</a>
                </li>
            @endforeach
        </ul>
        
        <h4 class="mt-4">Правильное ударение в словах:</h4>
        <ul class="list related">
            @php
            $ids = [];
            for($i = 0; $i < 30; $i++)
                $ids[] = rand(0, 496);
            @endphp
            @foreach(\App\Models\Accent::find($ids) as $word)
                <li>
                    <a href="{{ route('accent', $word->slug) }}">{!! $word->title !!}</a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
