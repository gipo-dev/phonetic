<?php

use App\Http\Controllers\AccentController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\SpellingController;
use App\Http\Controllers\WordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(\route('index'));
});

Route::group(['prefix' => 'phonetic'], function () {

    Route::get('/', [IndexController::class, 'index'])->name('index');
    Route::post('/search', [WordController::class, 'search'])->name('search');
    Route::get('/words/{letter?}', [WordController::class, 'words'])->name('words');
    Route::get('/pravopisanie/list', [SpellingController::class, 'list'])->name('spelling.list');
    Route::get('/pravopisanie/{word}', [SpellingController::class, 'show'])->name('spelling');
    Route::get('/udarenie/list', [AccentController::class, 'list'])->name('accent.list');
    Route::get('/udarenie/{word}', [AccentController::class, 'show'])->name('accent');
    Route::get('/proverochnoe/list', [WordController::class, 'specials'])->name('special.list');
    Route::get('/proverochnoe/{word}', [WordController::class, 'special'])->name('special');
    Route::get('/sostav/{word}', [WordController::class, 'composition'])->name('composition');
    Route::get('/open/{type}/{id}', [WordController::class, 'open'])->name('open');
    Route::get('/{word}', [WordController::class, 'result'])->name('word');

});
