<?php

declare(strict_types=1);

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{users}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Edit'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Platform > Words > Add
Route::screen('words/add', \App\Orchid\Screens\Word\WordCreateScreen::class)
    ->name('platform.word.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.word.list')->push('Создание');
    });

// Platform > Words > Edit
Route::screen('words/{word}', \App\Orchid\Screens\Word\WordEditScreen::class)
    ->name('platform.word.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.word.list')->push('Редактирование');
    });

// Platform > Words > List
Route::screen('words', \App\Orchid\Screens\Word\WordListScreen::class)
    ->name('platform.word.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->push('Фонетический разбор', route('platform.word.list'));
    });

// Platform > SpecialWords > Add
Route::screen('specials/add', \App\Orchid\Screens\SpecialWord\SpecialWordCreateScreen::class)
    ->name('platform.specials.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.specials.list')->push('Создание');
    });

// Platform > SpecialWords > Edit
Route::screen('specials/{word}', \App\Orchid\Screens\SpecialWord\SpecialWordEditScreen::class)
    ->name('platform.specials.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.specials.list')->push('Редактирование');
    });

// Platform > SpecialWords > List
Route::screen('specials', \App\Orchid\Screens\SpecialWord\SpecialWordListScreen::class)
    ->name('platform.specials.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->push('Проверочные слова', route('platform.specials.list'));
    });

// Platform > Spelling > Add
Route::screen('spelling/add', \App\Orchid\Screens\Spelling\SpellingCreateScreen::class)
    ->name('platform.spelling.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.spelling.list')->push('Создание');
    });

// Platform > Spelling > Edit
Route::screen('spelling/{word}', \App\Orchid\Screens\Spelling\SpellingEditScreen::class)
    ->name('platform.spelling.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.spelling.list')->push('Редактирование');
    });

// Platform > Spelling > List
Route::screen('spelling', \App\Orchid\Screens\Spelling\SpellingListScreen::class)
    ->name('platform.spelling.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->push('Правописание', route('platform.spelling.list'));
    });
